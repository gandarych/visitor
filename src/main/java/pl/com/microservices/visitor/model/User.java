package eu.microwebservices.user.model;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

// import org.springframework.data.elasticsearch.annotations.Document;


@Data
//@Document(indexName = "userdb", type = "user")
@Entity
@Table(name = "user")
public class User {

   /**
    * 
// https://projectlombok.org/features/Data
// https://stormit.pl/lombok/#data
// https://medium.com/@ashish_fagna/getting-started-with-elasticsearch-creating-indices-inserting-values-and-retrieving-data-e3122e9b12c6
// https://stackoverflow.com/questions/59278195/spel-used-in-document-indexname-with-spring-data-elasticsearch-and-spring-boot
    */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Setter(AccessLevel.PACKAGE)
    private String content;

    @Setter(AccessLevel.PACKAGE)
    private String author;

    @Setter(AccessLevel.PACKAGE)
    private String about;

    public User(Long id) {
        this.id = id;
    }

    public User(String content, String author, String about) {
        this.name = name;
        this.score = score;
        this.userid = userid;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setAbout(String about) {
        this.about = about;
    }


@FunctionalInterface 
interface Walkable {
    public String walk();
}

@FunctionalInterface  
interface Sayable{  
    void say(String msg);
} 

@FunctionalInterface 
interface Talkable extends Sayable {
    default public String talk() {}
    default public String ask() {}
    default public String reply() {}
}

/*
        Walkable walkWithLambda = () -> {
            return "I want to walk around.";
        };
        walkWithLambda.walk();

        Talkable talkWithLambda = (msg1, msg2) -> {
            System.out.println("%s %s", msg1, msg2);
        };
        talkWithLambda.ask("I want to talk to somebody.", "Could You, please?");


        Sayable sayWithLambda = (msg) - > {
            System.out.println(msg);
        };
        sayWithLambda.say("I want to say something important!");


        Sayable sayWithLambda = () -> {
            return "";
        };
        sayWithLambda.say();





Collections.sort(listOfPerson, (Person o1, Person o2) -> {
            return o1.getAge() - o2.getAge();
        });
        // Use forEach method added in java 8
        System.out.println(" sort persons by age in ascending order");
        listOfPerson.forEach(
           (person) -> System.out.println(" Person name : " + person.getName()));
        }

*/

    
} 
