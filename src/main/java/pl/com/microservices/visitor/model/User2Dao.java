package eu.microwebservices.user.model;

import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import eu.microwebservices.user.model.User;
import java.util.List;

import org.springframework.dao.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.servlet.tags.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
@Transactional
public interface UserDao extends CrudRepository<User, Long> {

   /**
   * This method will find an User instance in the database by its Id.
   * Note that this method is not implemented and its working code will be
   * automagically generated from its signature by Spring Data JPA.
   */
    @Query("SELECT us FROM User us WHERE us.id=(:pId)")
    User findById(@PathVariable("pId") BigInteger pId);

  /**
   * This method will find an User instance in the database by its email.
   * Note that this method is not implemented and its working code will be
   * automagically generated from its signature by Spring Data JPA.
   */
    @Query("SELECT us FROM User us WHERE us.email=(:pEmail)")
    User findByEmail(@PathVariable("pEmail") String pEmail);
  
//   /**
//   * This method will find an User instance in the database by its full name.
//   * Note that this method is not implemented and its working code will be
//   * automagically generated from its signature by Spring Data JPA.
//   */
//    @Query("SELECT us FROM User us WHERE us.fullName=(:pFullName)")
//    User findByFullName(@PathVariable("pFullName") String pFullName);

}
