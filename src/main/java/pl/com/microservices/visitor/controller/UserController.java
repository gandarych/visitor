package eu.microwebservices.user.controller;

import java.math.BigInteger;

// import java.util.Arrays;
// import java.util.stream.Stream;
import java.util.Optional;
import java.util.logging.Logger;

import eu.microwebservices.user.model.User;
import eu.microwebservices.user.model.UserDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.*;
import org.springframework.data.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.servlet.tags.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class UserController {
   /**
    * HOW TO TEST:
    * $ mvn spring-boot:run
    * http://localhost:8087/
    * Use the following urls:
    *    /user/create?name=[name]&score=[score]:
    *          create a new user with an auto-generated id and price and name as passed values.
    *    /user/delete?id=[id]:
    *          delete the user with the passed id.
    *    /user/update?id=[id]&email=[email]&name=[name]:
    *          update the price and the name for the user indentified by the passed id.
    */

  @Autowired
  private UserDao userDao;

	@GetMapping(value = "/user/{id}")
	public User getUser(@PathVariable("id") /* BigInteger */ Long id) {
        User user = null;
        try {
            Optional<User> UserOptional =
                    Optional.ofNullable(userDao.findById(BigInteger.valueOf(id)));
            if (UserOptional.isPresent()) {
                user = UserOptional.get();;
            }
        } catch (Exception ex) {
            // @TODO @FIXME
        }
	    return user;
	}
  
  /**
   * /create-user  --> Create a new user and save it in the database.
   * It is not secure operation here! There is no validation here!  See https://www.owasp.org
   * It is only for REST educational purposes...
   * 
   * @param name User's name
   * @param score User's score
   * @return A string describing if the user is successfully created or not.
   */
  @RequestMapping("/user/create")
  @ResponseBody
  public String create(String content, String author, String about) {
    User user = null;
    try {
      user = new User(content, author, about);
      userDao.save(user);
    }
    catch (Exception ex) {
      return "Error while creating the user: " + ex.toString();
    }
    return "User created succesfully!! (id = " + user.getId() + ")";
  }
  
  /**
   * /delete-user  --> Delete the user having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id of the user to delete
   * @return A string describing if the user is successfully deleted or not.
   */
  @RequestMapping("/user/delete")
  @ResponseBody
  public String delete(Long id) {
    try {
      User user = new User(id);
      userDao.delete(user);
    }
    catch (Exception ex) {
      return "Error while deleting the user: " + ex.toString();
    }
    return "User deleted successfully!!";
  }
  
  /**
   * /update-user  --> Update the price and the name for the user in the database 
   * having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id for the user to update.
   * @param name The new name.
   * @param score The new score.
   * @return A string describing if the user is successfully updated or not.
   */
  @RequestMapping("/user/update")
  @ResponseBody
  public String updateUser(Long id, String content, String author, String about) {
    User user = null;
    try {
		Optional<User> UserOptional =
                Optional.ofNullable(userDao.findById(BigInteger.valueOf(id)));
		if (UserOptional.isPresent()) {
		      user = UserOptional.get();
		      user.setContent(content);
		      user.setAuthor(author);
		      user.setAbout(about);
		      userDao.save(user);
	    }
    } catch (Exception ex) {
      return "Error while updating the user: " + ex.toString();
    }
    return "User updated successfully!!";
  }


/*
        Walkable walkWithLambda = () -> {
            return "I want to walk around.";
        };
        walkWithLambda.walk();

        Talkable talkWithLambda = (msg1, msg2) -> {
            System.out.println("%s %s", msg1, msg2);
        };
        talkWithLambda.ask("I want to talk to somebody.", "Could You, please?");


        Sayable sayWithLambda = (msg) - > {
            System.out.println(msg);
        };
        sayWithLambda.say("I want to say something important!");


        Sayable sayWithLambda = () -> {
            return "";
        };
        sayWithLambda.say();





Collections.sort(listOfPerson, (Person o1, Person o2) -> {
            return o1.getAge() - o2.getAge();
        });
        // Use forEach method added in java 8
        System.out.println(" sort persons by age in ascending order");
        listOfPerson.forEach(
           (person) -> System.out.println(" Person name : " + person.getName()));
        }

*/
  
}
