package eu.microwebservices.visitor.controller;

import java.math.BigInteger;

// import java.util.Arrays;
// import java.util.stream.Stream;
import java.util.Optional;
import java.util.logging.Logger;

import eu.microwebservices.visitor.model.Visitor;
import eu.microwebservices.visitor.model.VisitorDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.*;
import org.springframework.data.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.servlet.tags.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class VisitorController {
   /**
    * HOW TO TEST:
    * $ mvn spring-boot:run
    * http://localhost:8087/
    * Use the following urls:
    *    /visitor/create?name=[name]&score=[score]:
    *          create a new visitor with an auto-generated id and price and name as passed values.
    *    /visitor/delete?id=[id]:
    *          delete the visitor with the passed id.
    *    /visitor/update?id=[id]&email=[email]&name=[name]:
    *          update the price and the name for the visitor indentified by the passed id.
    */

  @Autowired
  private VisitorDao visitorDao;

	@GetMapping(value = "/visitor/{id}")
	public Visitor getVisitor(@PathVariable("id") /* BigInteger */ Long id) {
        Visitor visitor = null;
        try {
            Optional<Visitor> VisitorOptional =
                    Optional.ofNullable(visitorDao.findById(BigInteger.valueOf(id)));
            if (VisitorOptional.isPresent()) {
                visitor = VisitorOptional.get();;
            }
        } catch (Exception ex) {
            // @TODO @FIXME
        }
	    return visitor;
	}
  
  /**
   * /create-visitor  --> Create a new visitor and save it in the database.
   * It is not secure operation here! There is no validation here!  See https://www.owasp.org
   * It is only for REST educational purposes...
   * 
   * @param name Visitor's name
   * @param score Visitor's score
   * @return A string describing if the visitor is successfully created or not.
   */
  @RequestMapping("/visitor/create")
  @ResponseBody
  public String create(String content, String author, String about) {
    Visitor visitor = null;
    try {
      visitor = new Visitor(content, author, about);
      visitorDao.save(visitor);
    }
    catch (Exception ex) {
      return "Error while creating the visitor: " + ex.toString();
    }
    return "User created succesfully!! (id = " + visitor.getId() + ")";
  }
  
  /**
   * /delete-visitor  --> Delete the visitor having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id of the visitor to delete
   * @return A string describing if the visitor is successfully deleted or not.
   */
  @RequestMapping("/visitor/delete")
  @ResponseBody
  public String delete(Long id) {
    try {
      Visitor visitor = new Visitor(id);
      visitorDao.delete(visitor);
    }
    catch (Exception ex) {
      return "Error while deleting the visitor: " + ex.toString();
    }
    return "Visitor deleted successfully!!";
  }
  
  /**
   * /update-visitor  --> Update the price and the name for the visitor in the database 
   * having the passed id.
   * It is not secure operation here! There is no validation here!
   * It is only for REST educational purposes...
   * 
   * @param id The id for the visitor to update.
   * @param name The new name.
   * @param score The new score.
   * @return A string describing if the visitor is successfully updated or not.
   */
  @RequestMapping("/visitor/update")
  @ResponseBody
  public String updateVisitor(Long id, String content, String author, String about) {
    Visitor visitor = null;
    try {
		Optional<Visitor> VisitorOptional =
                Optional.ofNullable(visitorDao.findById(BigInteger.valueOf(id)));
		if (VisitorOptional.isPresent()) {
		      visitor = VisitorOptional.get();
		      visitor.setContent(content);
		      visitor.setAuthor(author);
		      visitor.setAbout(about);
		      visitorDao.save(visitor);
	    }
    } catch (Exception ex) {
      return "Error while updating the visitor: " + ex.toString();
    }
    return "Visitor updated successfully!!";
  }
  
}
